//fetch all books from db
var fetchBooks =function (vueElem) {
    axios.get("/books").then(function (response) {
        vueElem.noRecordAlert =false;
        if(!response.data.length){
            vueElem.noRecordAlert =true;
        }else{
            vueElem.books = _.sortBy(response.data,"title");
            vueElem.persons = [];
            vueElem.personLoanHistory=[];
        }
    }.bind(vueElem));
}
//fetch all persons from db
var fetchPersons =function (vueElem) {
    axios.get("/persons").then(function (response) {
        vueElem.noRecordAlert =false;
        if(!response.data.length){
            vueElem.noRecordAlert =true;
        }else{
            vueElem.persons = _.sortBy(response.data, 'name');
            vueElem.books = [];
            vueElem.bookLoanHistory=[];
        }
    }.bind(vueElem));
}
//fetch all book loan history by book id
var fetchBookLoanHistory = function(book,vueElem){
    //set the previous selected book ischecked to false if there is one.
    var previousSelectedBook = _.find(vueElem.books, function (book) {
        return book.isChecked;
    });
    if (previousSelectedBook) {
        previousSelectedBook.isChecked = false;
    }
    book.isChecked = true;
    vueElem.bookLoanHistory = [];
    axios.get("/book/" + book.id).then(function (response) {
        vueElem.bookLoanHistory =response.data;
    }.bind(vueElem));
}
//fetch all person loan history by person id
var fetchPersonLoanHistory =function (person,vueElem) {
    //set the previous selected person ischecked to false if there is one.
    var previousSelectedPerson = _.find(vueElem.persons, function (person) {
        return person.isChecked;
    });
    if (previousSelectedPerson) {
        previousSelectedPerson.isChecked = false;
    }
    person.isChecked = true;
    vueElem.personLoanHistory = [];
    axios.get("/personLoanRecords/" + person.id).then(function (response) {
        vueElem.personLoanHistory =response.data;
    }.bind(vueElem));
}
window.onload = function () {
    var app = new Vue({
        el: '#root',
        data: {
            books: [],
            persons: [],
            personLoanHistory: [],
            bookLoanHistory:[],
            noRecordAlert:false
        },
        methods: {
            fetchBooks(){fetchBooks(this)},
            fetchPersons(){fetchPersons(this)} ,
            fetchBookLoanHistory(book){fetchBookLoanHistory(book,this)} ,
            fetchPersonLoanHistory(person){fetchPersonLoanHistory(person,this)} ,
            goToAddNewRecordPage: function () {
                window.location.href = "/addRecords";
            }
        },
        filters: {
            moment: function (date) {
                return moment(date).format('MMMM Do YYYY');
            }
        }

    });
}