//update vue element to display person panel
var showNewPersonPanel =function (vueElem) {
    vueElem.showAddPerson = true;
    vueElem.showAddBook = false;
    vueElem.showAddLoanRecord = false;
    vueElem.errors = [];
    vueElem.response = "";
}    
//update vue element to display book panel
var showNewBookPanel =function (vueElem) {
    vueElem.showAddBook = true;
    vueElem.showAddLoanRecord = false;
    vueElem.showAddPerson = false;
    vueElem.errors = [];
    vueElem.response = "";
}
//update vue element to display loan record panel
var showNewLoanRecordPanel= function (vueElem) {
    vueElem.showAddLoanRecord = true;
    vueElem.showAddBook = false;
    vueElem.showAddPerson = false;
    vueElem.errors = [];
    vueElem.response = "";
    //fetch data
    vueElem.fetchBooks();
    vueElem.fetchPersons();
}
//validator for add new person record
var personFormValidator= function (vueElem) {
    vueElem.errors = [];
    //form validator
    if (!vueElem.newPerson.name) {
        var personFormError = "Please fill all reqiured fields";
        vueElem.errors.push(personFormError);
    }
    if (vueElem.newPerson.emailAddress && vueElem.newPerson.emailAddress.indexOf("@") < 0) {
        var personFormError = "You email is not in correct format";
        vueElem.errors.push(personFormError);
    }
}
//add new person record into db
var addNewPerson =function (vueElem) {
    //run validator
    personFormValidator(vueElem);

    if (!vueElem.errors.length) {
        axios({
            method: 'post',
            url: '/person',
            data: vueElem.newPerson
        }).then(function (response) {
            vueElem.response = response.data;
            vueElem.newPerson = {};
        }.bind(vueElem))
    }
}
//validator for add new book record
var bookFormValidator=function (vueElem) {
    vueElem.errors = [];
    //form validator
    if (!vueElem.newBook.title || !vueElem.newBook.author || !vueElem.newBook.ISBN) {
        var bookFormError = "Please fill all reqiured fields";
        vueElem.errors.push(bookFormError);
    }
}
//add new book into db
var addNewBook =function (vueElem) {
    //run form validator
    bookFormValidator(vueElem);
    if (!vueElem.errors.length) {
        axios({
            method: 'post',
            url: '/book',
            data: vueElem.newBook
        }).then(function (response) {
            vueElem.response = response.data;
            vueElem.newBook = {};
            vueElem.errors = [];
        }.bind(vueElem))
    }
}
//fetch all book records from db
var fetchBooks =function (vueElem) {
    axios.get("/books").then(function (response) {
        
        vueElem.books =  _.sortBy(response.data,"title");
    }.bind(vueElem));
}
//fetch all person records from db
var fetchPersons = function (vueElem) {
    axios.get("/persons").then(function (response) {
        vueElem.persons = _.sortBy(response.data, 'name');
    }.bind(vueElem));
}
//validator for loan record
var LoanBooksValidator =function (vueElem,bookIds) {
    vueElem.errors = [];
    if (!bookIds.length) {
        var loanBooksError = "Please select at least one book";
        vueElem.errors.push(loanBooksError);
    }
    if (!vueElem.selected) {
        var loanBooksError = "Please select a borrower";
        vueElem.errors.push(loanBooksError);
    }
}
//add loan record to db
var loanBooks =function (vueElem) {
    var loanBooks = _.filter(vueElem.books, function (book) {
        return book.isChecked;
    });
    var bookIds = _.map(loanBooks, function (book) {
        return book.id;
    });
    //run validator
    LoanBooksValidator(vueElem,bookIds);

    if (!vueElem.errors.length) {
        axios({
            method: 'post',
            url: '/loanRecord',
            data: {
                bookIds: bookIds,
                borrowerId: vueElem.selected
            }
        }).then(function (response) {
            vueElem.response = response.data;
            vueElem.selected = "";
            vueElem.books = _.forEach(vueElem.books, function (book) {
                book.isChecked = false;
            })
        }.bind(vueElem));
    }
}
    window.onload = function () {
        var app = new Vue({
            el: '#addRecords',
            data: {
                books: [],
                persons: [],
                errors: [],
                selected: "",
                newPerson: {},
                newBook: {},
                showAddPerson: false,
                showAddBook: false,
                showAddLoanRecord: false,
                response: ""
            },
            mounted() {},
            methods: {
                //#region switch from add new person/book/loan record
                showNewPersonPanel(){showNewPersonPanel(this)},
                showNewBookPanel () {showNewBookPanel(this)},
                showNewLoanRecordPanel(){showNewLoanRecordPanel(this)},
                //#endregion
                addNewPerson(){addNewPerson(this)},
                addNewBook(){addNewBook(this)},
                fetchBooks(){fetchBooks(this)},
                fetchPersons(){fetchPersons(this)},
                loanBooks(){loanBooks(this)},
                goToHomePage: function () {
                    window.location.href = "/";
                }
            }
        });
    }