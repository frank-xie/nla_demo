package com.libraryweb.webapp.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class PersonBorrowRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Date borrowDate;
    @ElementCollection
    public List<Long> bookIds;

    @ManyToMany
    public List<Book> books;
    public Long borrowerId;
    @ManyToOne
    public Person person;


    public PersonBorrowRecord() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
