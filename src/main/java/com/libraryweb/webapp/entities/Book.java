package com.libraryweb.webapp.entities;

import javax.persistence.*;

@Entity
public class Book {
    @Id
    @GeneratedValue
    private long id;

    public  String title;

    public  String author;
    public long ISBN;

    public Book() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
