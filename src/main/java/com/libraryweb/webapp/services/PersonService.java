package com.libraryweb.webapp.services;

import com.libraryweb.webapp.entities.Person;
import com.libraryweb.webapp.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
//PersonService
//getAllPerson: get all person records from database
//getPersonById: get person record by person id
//add: add a person record into database
@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;

    public List<Person> getAllPerson() {
        return personRepository.findAll();
    }

    public Person getPersonById(Long id) {
        return personRepository.findById(id).get();
    }

    public void add(Person person) {
        personRepository.save(person);
    }
}
