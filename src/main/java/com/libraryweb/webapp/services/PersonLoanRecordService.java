package com.libraryweb.webapp.services;

import com.libraryweb.webapp.entities.PersonBorrowRecord;
import com.libraryweb.webapp.repositories.PersonLoanRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
//PersonLoanRecordService
//getAllRecords: get all person loan records from database
//add: add new person loan record into database
//getBookLoanRecords: get all person loan records related to the book by book id
//getPersonLoanRecords: get all person loan records related to the person by person id
//removeAll: remove all person loan records form database
@Service
public class PersonLoanRecordService {
    @Autowired
    private PersonLoanRecordRepository personLoanRecordRepository;

    public List<PersonBorrowRecord> getAllRecords() {
        return personLoanRecordRepository.findAll();
    }

    public void add(PersonBorrowRecord record) {
        personLoanRecordRepository.save(record);
    }

    public List<PersonBorrowRecord> getBookLoanRecords(Long id) {
        return personLoanRecordRepository.findAllLoanRecordsByBookId(id);
    }

    public List<PersonBorrowRecord> getPersonLoanRecords(Long id) {
        return personLoanRecordRepository.findAllLoanRecordsByPersonId(id);
    }

    public void removeAll() {
        personLoanRecordRepository.deleteAll();
    }
}
