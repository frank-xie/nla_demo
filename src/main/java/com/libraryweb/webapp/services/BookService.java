package com.libraryweb.webapp.services;

import com.libraryweb.webapp.entities.Book;
import com.libraryweb.webapp.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
//book service
//getAllBooks: get all books from database
//add: add a new book into database
//getBooks: get books by list of book id
@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public void add(Book book) {
        bookRepository.save(book);
    }

    public List<Book> getBooks(List<Long> bookIds) {
        return bookRepository.findAllById(bookIds);
    }
}
