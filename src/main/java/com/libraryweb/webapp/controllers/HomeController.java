package com.libraryweb.webapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping(value="/")
    public String index(){
        return "index";
    }

    @GetMapping(value="/addRecords")
    public String addRecords(){return "addRecords";}

}
