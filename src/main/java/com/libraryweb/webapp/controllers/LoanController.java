package com.libraryweb.webapp.controllers;

import com.libraryweb.webapp.entities.Person;
import com.libraryweb.webapp.entities.PersonBorrowRecord;
import com.libraryweb.webapp.services.BookService;
import com.libraryweb.webapp.services.PersonLoanRecordService;
import com.libraryweb.webapp.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class LoanController {
    @Autowired
    private PersonLoanRecordService personLoanRecordService;
    @Autowired
    private PersonService personService;
    @Autowired
    private BookService bookService;

    @GetMapping(value ="/allLoanRecords")
    public List<PersonBorrowRecord> allRecords(){ return  personLoanRecordService.getAllRecords();}

    @PostMapping(value="/loanRecord")
    public String addRecord(@RequestBody PersonBorrowRecord personBorrowRecord){
        Date loanDate =  new Date();
        Person borrower= personService.getPersonById(personBorrowRecord.borrowerId);
        if (borrower == null)
            return null;

        personBorrowRecord.borrowDate=loanDate;
        personBorrowRecord.person=borrower;
        personLoanRecordService.add(personBorrowRecord);
        personBorrowRecord.books  = bookService.getBooks(personBorrowRecord.bookIds);
        personLoanRecordService.add(personBorrowRecord);
        return "Added new loan record successfully.";
    }

    @PostMapping(value ="/removeAllLoanRecord")
    public String removeAll(){
        personLoanRecordService.removeAll();
        return "Removed all records";
    }

}
