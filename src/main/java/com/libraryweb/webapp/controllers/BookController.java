package com.libraryweb.webapp.controllers;

import com.libraryweb.webapp.entities.Book;
import com.libraryweb.webapp.entities.PersonBorrowRecord;
import com.libraryweb.webapp.services.BookService;
import com.libraryweb.webapp.services.PersonLoanRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;
    @Autowired
    private PersonLoanRecordService personLoanRecordService;

    @GetMapping(value ="/books")
    public List<Book> books(){return  bookService.getAllBooks();}

    @GetMapping(value ="/book/{id}")
    public List<PersonBorrowRecord> bookLoanRecords (@PathVariable Long id){
        return personLoanRecordService.getBookLoanRecords(id);
    }
    @PostMapping(value = "/book")
    public String addBook(@RequestBody Book book){
        if(book.title.length()==0 ||book.author.length()==0|| book.ISBN<0)
            return null;
        
        bookService.add(book);
        return "Success added a book";
    }

}
