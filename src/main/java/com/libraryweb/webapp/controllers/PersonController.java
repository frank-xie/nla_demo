package com.libraryweb.webapp.controllers;

import com.libraryweb.webapp.entities.PersonBorrowRecord;
import com.libraryweb.webapp.entities.Person;
import com.libraryweb.webapp.services.PersonLoanRecordService;
import com.libraryweb.webapp.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {
    @Autowired
    PersonService personService;
    @Autowired
    private PersonLoanRecordService personLoanRecordService;
    @GetMapping(value ="/persons")
    public List<Person> persons(){return personService.getAllPerson();}

    @GetMapping(value ="/personLoanRecords/{id}")
    public List<PersonBorrowRecord> personLoanRecords (@PathVariable Long id){
        return personLoanRecordService.getPersonLoanRecords(id);
    }

    @GetMapping(value="/person/{id}")
    public Person getPersonById(@PathVariable Long id){
        return personService.getPersonById(id);
    }

    @PostMapping(value = "/person")
    public String addPerson(@RequestBody Person person){
        if(person.name.length()==0)
            return null;
        personService.add(person);
        return "Success added a person";
    }
}
