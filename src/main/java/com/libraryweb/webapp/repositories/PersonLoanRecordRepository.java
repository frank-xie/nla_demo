package com.libraryweb.webapp.repositories;

import com.libraryweb.webapp.entities.PersonBorrowRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonLoanRecordRepository extends JpaRepository<PersonBorrowRecord,Long> {


    @Query("select r from PersonBorrowRecord r where :bookId member r.bookIds")
    List<PersonBorrowRecord> findAllLoanRecordsByBookId(@Param("bookId") Long id);

    @Query("select r from PersonBorrowRecord r where :borrowerId = r.borrowerId")
    List<PersonBorrowRecord> findAllLoanRecordsByPersonId(@Param("borrowerId") Long id);
}
